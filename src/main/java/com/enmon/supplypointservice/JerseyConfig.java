package com.enmon.supplypointservice;

import com.enmon.supplypointservice.resource.CustomerResource;
import com.enmon.supplypointservice.resource.SupplyPointResource;
import com.enmon.supplypointservice.resource.UserResource;
import com.enmon.supplypointservice.util.AccessDeniedMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {
	
	public JerseyConfig() {
		register(UserResource.class);
		register(SupplyPointResource.class);
		register(AccessDeniedMapper.class);
		register(CustomerResource.class);
		
		//TODO: do I really need to do it manually?
	}
	
}
