package com.enmon.supplypointservice.auth;

import com.enmon.supplypointservice.domain.User;
import com.enmon.supplypointservice.auth.user.UserType;
import com.enmon.supplypointservice.db.RepositoryFacade;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
	
	private final RepositoryFacade repositoryFacade;
	
	public JwtAuthorizationFilter(AuthenticationManager authenticationManager, RepositoryFacade repositoryFacade) {
		super(authenticationManager);
		this.repositoryFacade = repositoryFacade;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
		String header = request.getHeader(SecurityConstants.HEADER_STRING);
		if (header == null || !header.startsWith(SecurityConstants.TOKEN_PREFIX)) {
			chain.doFilter(request, response);
			return;
		}
		UsernamePasswordAuthenticationToken authentication = getAuthentication(header);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(request, response);
	}
	
	private UsernamePasswordAuthenticationToken getAuthentication(String tokenHeader) {
		Claims body = Jwts.parser()
				.setSigningKey(SecurityConstants.SECRET.getBytes())
				.parseClaimsJws(tokenHeader.replace(SecurityConstants.TOKEN_PREFIX, ""))
				.getBody();
		String userString = body.getSubject();
		
		User user = repositoryFacade.getUserRepository().findOneByEmail(userString);
		//TODO: consider moving this to JwTAuthenticationToken class and reading from the JWT token to save the DB calls
		if (user != null) {
			
			UserType userType = user.getType();
			List<?> roles = userType.getAuthorities();
			List<?> customers = null;
			if (UserType.USER.equals(userType) || UserType.ADMIN.equals(userType)) {
				customers = Arrays.asList(user.getCustomerId());
			}
			
			Collection<GrantedAuthority> authorities = roles.stream()
					.map(role -> new SimpleGrantedAuthority(role.toString()))
					.collect(Collectors.toList());
			return new UsernamePasswordAuthenticationToken(user, customers, authorities);
		}
		
		return null;
	}
	
}
