package com.enmon.supplypointservice.auth;

public class SecurityConstants {

    public static final String SECRET = "SecretKeyToGenJWTs"; //FIXME: needs to be stored somewhere else
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
	
	public static final String USERS_SIGN_UP = "/v1/users/sign-up";
}
