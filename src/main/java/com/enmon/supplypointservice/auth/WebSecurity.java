package com.enmon.supplypointservice.auth;

import com.enmon.supplypointservice.db.RepositoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Collection;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	private final UserDetailsService userDetailsService;
	
	private final RepositoryFacade repositoryFacade;
	
	@Autowired
	public WebSecurity(BCryptPasswordEncoder bCryptPasswordEncoder, UserDetailsService userDetailsService, RepositoryFacade repositoryFacade) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userDetailsService = userDetailsService;
		this.repositoryFacade = repositoryFacade;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests()
				.antMatchers("/v1/customers/{customerId}/**").access("@webSecurity.checkUserCanAccessCustomer(authentication,#customerId)")
				.antMatchers("/v1/users/{customerId}/**").access("@webSecurity.checkUserCanAccessCustomer(authentication,#customerId)")
				.anyRequest().authenticated()
				.and()
				.addFilter(new JwtAuthenticationFilter(authenticationManager()))
				.addFilter(new JwtAuthorizationFilter(authenticationManager(), repositoryFacade))
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
		
	}
	
	public boolean checkUserCanAccessCustomer(Authentication authentication, String customerId) {
		if (authentication instanceof AnonymousAuthenticationToken) {
			return false;
		}
		
		if (authentication.getAuthorities().stream().anyMatch(authority -> "MAGIC".equals(authority.toString()))) { //TODO: constant
			return true;
		} else {
			Collection<String> credentials = (Collection<String>) authentication.getCredentials();
			return credentials.contains(customerId);
		}
	}
	
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}
}
