package com.enmon.supplypointservice.auth.user;


import com.enmon.supplypointservice.auth.user.UserTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target(value = {TYPE, METHOD, FIELD, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {UserTypeValidator.class})
public @interface IsAllowedUserType {
	String message() default "This user type is not allowed";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};
}

