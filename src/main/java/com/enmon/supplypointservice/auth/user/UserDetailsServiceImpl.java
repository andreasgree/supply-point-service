package com.enmon.supplypointservice.auth.user;

import com.enmon.supplypointservice.db.UserRepository;
import com.enmon.supplypointservice.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	
	private final UserRepository userRepository;
	
	@Autowired
	public UserDetailsServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userRepository.findOneByEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		
		//FIXME: we don't need the WithEmail entity probably
		return new UserDetailsWithEmail(user.getEmail(), user.getPassword(), user.getEmail(), new ArrayList<>());// Arrays.asList(new SimpleGrantedAuthority("USER")));
	}
}
