package com.enmon.supplypointservice.auth.user;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class UserDetailsWithEmail extends User {
	
	private final String email;
	
	public UserDetailsWithEmail(String username, String password, String email, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
}
