package com.enmon.supplypointservice.auth.user;

import java.util.Arrays;
import java.util.List;

public enum UserType {
	MAGIC(false, "MAGIC", "ADMIN", "USER"), USER(true, "USER"), ADMIN(true, "USER", "ADMIN");
	
	UserType(boolean canBeCreated, String... authorities) {
		this.canBeCreated = canBeCreated;
		this.authorities = Arrays.asList(authorities);
	}
	
	private boolean canBeCreated;
	
	private List<String> authorities;
	
	public boolean getCanBeCreated() {
		return canBeCreated;
	}
	
	public List<String> getAuthorities() {
		return authorities;
	}
}
