package com.enmon.supplypointservice.auth.user;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserTypeValidator implements ConstraintValidator<IsAllowedUserType, UserType> {
	@Override
	public void initialize(IsAllowedUserType constraintAnnotation) {
	
	}
	
	@Override
	public boolean isValid(UserType userType, ConstraintValidatorContext context) {
		return userType.getCanBeCreated();
	}
}
