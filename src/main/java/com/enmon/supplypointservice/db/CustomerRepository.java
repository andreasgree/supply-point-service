package com.enmon.supplypointservice.db;

import com.enmon.supplypointservice.domain.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {
}
