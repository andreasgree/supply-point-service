package com.enmon.supplypointservice.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RepositoryFacade {
	
	private final SupplyPointRepository supplyPointRepository;
	
	private final UserRepository userRepository;
	
	private final CustomerRepository customerRepository;
	
	@Autowired
	public RepositoryFacade(SupplyPointRepository supplyPointRepository, UserRepository userRepository, CustomerRepository customerRepository) {
		this.supplyPointRepository = supplyPointRepository;
		this.userRepository = userRepository;
		this.customerRepository = customerRepository;
	}
	
	public SupplyPointRepository getSupplyPointRepository() {
		return supplyPointRepository;
	}
	
	public UserRepository getUserRepository() {
		return userRepository;
	}
	
	public CustomerRepository getCustomerRepository() {
		return customerRepository;
	}
}
