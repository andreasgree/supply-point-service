package com.enmon.supplypointservice.db;

import com.enmon.supplypointservice.domain.SupplyPoint;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SupplyPointRepository extends SupplyPointRepositoryCustom, MongoRepository<SupplyPoint, String> {

}
