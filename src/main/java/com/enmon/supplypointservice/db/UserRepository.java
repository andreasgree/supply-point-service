package com.enmon.supplypointservice.db;

import com.enmon.supplypointservice.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

	User findOneByEmail(String email);
	
	User findOneByCustomerIdAndId(String customerId, String id);
}
