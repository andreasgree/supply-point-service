package com.enmon.supplypointservice.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "supplyPoints")
public class SupplyPoint {
	
	@Id
	private String id;
	
	private String name;
	
	private List<String> users;
	
	private Double area;
	
	private int workingHours;
	
	private List<Integer> workingDays;
	
	private List<String> tags;
	
	public List<Integer> getWorkingDays() {
		return workingDays;
	}
	
	public void setWorkingDays(List<Integer> workingDays) {
		this.workingDays = workingDays;
	}
	
	public List<String> getTags() {
		return tags;
	}
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<String> getUsers() {
		return users;
	}
	
	public void setUsers(List<String> users) {
		this.users = users;
	}
	
	public Double getArea() {
		return area;
	}
	
	public void setArea(Double area) {
		this.area = area;
	}
	
	public int getWorkingHours() {
		return workingHours;
	}
	
	public void setWorkingHours(int workingHours) {
		this.workingHours = workingHours;
	}
}
