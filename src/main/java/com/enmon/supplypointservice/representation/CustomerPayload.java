package com.enmon.supplypointservice.representation;

import org.hibernate.validator.constraints.NotEmpty;

public class CustomerPayload {
	
	@NotEmpty
	private String name;
	
	public String getName() {
		return name;
	}
}
