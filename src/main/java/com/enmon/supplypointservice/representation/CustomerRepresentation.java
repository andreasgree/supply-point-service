package com.enmon.supplypointservice.representation;

public class CustomerRepresentation {
	
	private String customerId;
	
	private String name;
	
	public CustomerRepresentation(String customerId, String name) {
		this.customerId = customerId;
		this.name = name;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	
	public String getName() {
		return name;
	}
}
