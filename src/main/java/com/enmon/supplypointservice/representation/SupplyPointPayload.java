package com.enmon.supplypointservice.representation;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class SupplyPointPayload {
	
	@NotEmpty
	private String name;
	
	private List<String> users;
	
	private Double area;
	
	private int workingHours;
	
	private List<Integer> workingDays;
	
	private List<String> tags;
	
	public String getName() {
		return name;
	}
	
	public List<String> getUsers() {
		return users;
	}
	
	public Double getArea() {
		return area;
	}
	
	public int getWorkingHours() {
		return workingHours;
	}
	
	public List<Integer> getWorkingDays() {
		return workingDays;
	}
	
	public List<String> getTags() {
		return tags;
	}
}
