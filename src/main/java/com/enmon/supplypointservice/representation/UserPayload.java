package com.enmon.supplypointservice.representation;

import com.enmon.supplypointservice.auth.user.UserType;
import com.enmon.supplypointservice.auth.user.IsAllowedUserType;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class UserPayload {
	
	@Email
	@NotEmpty
	private String email;
	
	@NotEmpty
	private String name;
	
	@NotEmpty
	private String password;
	
	@NotNull
	@IsAllowedUserType
	private UserType type;
	
	public String getEmail() {
		return email;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public UserType getType() {
		return type;
	}
}
