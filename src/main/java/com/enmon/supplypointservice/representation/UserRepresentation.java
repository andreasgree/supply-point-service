package com.enmon.supplypointservice.representation;

import com.enmon.supplypointservice.auth.user.UserType;

public class UserRepresentation {
	
	private String email;
	
	private String name;
	
	private String customerId;
	
	private UserType type;
	
	public UserRepresentation(String email, String name, String customerId, UserType type) {
		this.email = email;
		this.name = name;
		this.customerId = customerId;
		this.type = type;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	
	public UserType getType() {
		return type;
	}
}
