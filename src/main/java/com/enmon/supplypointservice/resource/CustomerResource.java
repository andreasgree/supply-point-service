package com.enmon.supplypointservice.resource;

import com.enmon.supplypointservice.representation.CustomerPayload;
import com.enmon.supplypointservice.representation.CustomerRepresentation;
import com.enmon.supplypointservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Component
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path(CustomerResource.PATH)
public class CustomerResource {
	
	public static final String PATH = "/v1/customers";
	
	private final CustomerService customerService;
	
	@Autowired
	public CustomerResource(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@PreAuthorize("hasAuthority('MAGIC')")
	@GET
	public Response getAllCustomers() {
		//TODO: paging
		//TODO: query param - parentId
		List<CustomerRepresentation> customers = customerService.getAllCustomers();
		return Response.ok(customers).build();
	}
	
	@PreAuthorize("hasAuthority('MAGIC')")
	@POST
	public Response createCustomer(@Valid CustomerPayload customerPayload) throws URISyntaxException {
		String customerId = customerService.createCustomer(customerPayload);
		URI uri = new URI(String.format("%s/%s", PATH, customerId)); //TODO: find some Spring magic for assembling the URI
		return Response.created(uri).build();
	}
	
	@GET
	@Path("/{customerId}")
	public Response getCustomer(@PathParam("customerId") String customerId) {
		Optional<CustomerRepresentation> customer = customerService.getCustomerById(customerId);
		
		if (customer.isPresent()) {
			return Response.ok(customer.get()).build();
		}
		
		return Response.status(Response.Status.NOT_FOUND).build();
		
	}
	
	
	
}
