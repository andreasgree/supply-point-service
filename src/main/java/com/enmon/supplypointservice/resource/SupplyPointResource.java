package com.enmon.supplypointservice.resource;

import com.enmon.supplypointservice.db.RepositoryFacade;
import com.enmon.supplypointservice.domain.SupplyPoint;
import com.enmon.supplypointservice.representation.SupplyPointPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@Component
@Produces(MediaType.APPLICATION_JSON)
@Path(SupplyPointResource.PATH)
public class SupplyPointResource {
	
	public static final String PATH = "/v1/supply-point";
	
	private final RepositoryFacade repositoryFacade;
	
	@Autowired
	public SupplyPointResource(RepositoryFacade repositoryFacade) {
		this.repositoryFacade = repositoryFacade;
	}
	
	@GET
	@Path("/{id}")
	public Response getSupplyPoint(@PathParam("id") String id) {
		//TODO: move to SupplyPointService
		SupplyPoint supplyPoint = repositoryFacade.getSupplyPointRepository().findOne(id);
		
		if (supplyPoint != null) {
			return Response.ok(supplyPoint).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	@POST
	public Response createNewSupplyPoint(@Valid SupplyPointPayload payload) throws URISyntaxException {
		//TODO: move to SupplyPointService, builder
		SupplyPoint supplyPoint = new SupplyPoint();
		supplyPoint.setName(payload.getName());
		supplyPoint.setUsers(payload.getUsers());
		supplyPoint.setArea(payload.getArea());
		supplyPoint.setWorkingHours(payload.getWorkingHours());
		supplyPoint.setWorkingDays(payload.getWorkingDays());
		supplyPoint.setTags(payload.getTags());
		
		SupplyPoint saved = repositoryFacade.getSupplyPointRepository().save(supplyPoint);
		URI uri = new URI(String.format("%s/%s", PATH, saved.getId())); //TODO: find some Spring magic for assembling the URI
		return Response.created(uri).build();
	}
}
