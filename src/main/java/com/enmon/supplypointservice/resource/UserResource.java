package com.enmon.supplypointservice.resource;

import com.enmon.supplypointservice.representation.UserPayload;
import com.enmon.supplypointservice.representation.UserRepresentation;
import com.enmon.supplypointservice.service.UserService;
import com.enmon.supplypointservice.util.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Component
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path(UserResource.PATH)
public class UserResource {
	
	public static final String PATH = "/v1/users";
	
	private final UserService userService;
	
	@Autowired
	public UserResource(UserService userService) {
		this.userService = userService;
	}
	
	@PreAuthorize("hasAnyAuthority('MAGIC','ADMIN')")
	@GET
	public Response getAllUsers() {
		//TODO: query params - customerId, parentId
		//TODO: paging
		List<UserRepresentation> userRepresentations = userService.getAllUsers();
		return Response.ok(userRepresentations).build();
	}
	
	@PreAuthorize("hasAnyAuthority('MAGIC','ADMIN')")
	@POST
	@Path("/{customerId}")
	public Response createNewUser(@PathParam("customerId") String customerId, @Valid UserPayload userPayload) throws URISyntaxException, UserAlreadyExistsException {
		String id = userService.createNewUserForCustomer(customerId, userPayload);
		
		URI uri = new URI(String.format("%s/%s/%s", PATH, customerId, id)); //TODO: find some Spring magic for assembling the URI
		return Response.created(uri).build();
	}
	
	@GET
	@Path("/{customerId}/{userId}")
	public Response getUser(@PathParam("customerId") String customerId, @PathParam("userId") String userId) {
		Optional<UserRepresentation> user = userService.getUserForCustomer(customerId, userId);
		if (user.isPresent()) {
			return Response.ok(user.get()).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}
