package com.enmon.supplypointservice.service;

import com.enmon.supplypointservice.db.RepositoryFacade;
import com.enmon.supplypointservice.domain.Customer;
import com.enmon.supplypointservice.representation.CustomerPayload;
import com.enmon.supplypointservice.representation.CustomerRepresentation;
import org.glassfish.jersey.internal.inject.Custom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerService {
	
	private final RepositoryFacade repositoryFacade;
	
	@Autowired
	public CustomerService(RepositoryFacade repositoryFacade) {
		this.repositoryFacade = repositoryFacade;
	}
	
	public List<CustomerRepresentation> getAllCustomers() {
		List<Customer> customers = repositoryFacade.getCustomerRepository().findAll();
		
		return customers.stream()
				.map(customer -> new CustomerRepresentation(customer.getCustomerId(), customer.getName()))
				.collect(Collectors.toList());
	}
	
	public String createCustomer(CustomerPayload customerPayload) {
		Customer customer = new Customer();
		customer.setName(customerPayload.getName());
		
		customer = repositoryFacade.getCustomerRepository().save(customer);
		return customer.getCustomerId();
	}
	
	public Optional<CustomerRepresentation> getCustomerById(String customerId) {
		Customer customer = repositoryFacade.getCustomerRepository().findOne(customerId);
		
		CustomerRepresentation representation = null;
		if (customer != null) {
			representation = new CustomerRepresentation(customer.getCustomerId(), customer.getName());
		}
		
		return Optional.ofNullable(representation);
		
	}
}
