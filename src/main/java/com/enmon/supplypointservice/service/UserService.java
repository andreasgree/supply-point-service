package com.enmon.supplypointservice.service;

import com.enmon.supplypointservice.db.RepositoryFacade;
import com.enmon.supplypointservice.domain.User;
import com.enmon.supplypointservice.representation.UserPayload;
import com.enmon.supplypointservice.representation.UserRepresentation;
import com.enmon.supplypointservice.util.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
	
	private final RepositoryFacade repositoryFacade;
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	public UserService(RepositoryFacade repositoryFacade, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.repositoryFacade = repositoryFacade;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	private static final UserRepresentation representationFromUser(User user) {
		return new UserRepresentation(user.getEmail(), user.getName(), user.getCustomerId(), user.getType());
	}
	
	public List<UserRepresentation> getAllUsers() {
		List<User> users = repositoryFacade.getUserRepository().findAll();
		return users.stream()
				.map(user -> representationFromUser(user))
				.collect(Collectors.toList());
	}
	
	public Optional<UserRepresentation> getUserForCustomer(String customerId, String userId) {
		User user = repositoryFacade.getUserRepository().findOneByCustomerIdAndId(customerId, userId);
		UserRepresentation representation = null;
		if (user != null) {
			representation = representationFromUser(user);
		}
		
		return Optional.ofNullable(representation);
	}
	
	public String createNewUserForCustomer(String customerId, UserPayload userPayload) throws UserAlreadyExistsException {
		User user = new User(); //TODO: builder pattern?
		user.setEmail(userPayload.getEmail());
		user.setName(userPayload.getName());
		user.setPassword(bCryptPasswordEncoder.encode(userPayload.getPassword()));
		user.setCustomerId(customerId);
		
		try {
			user = repositoryFacade.getUserRepository().save(user);
		} catch (DuplicateKeyException e) {
			//TODO: log properly
			throw new UserAlreadyExistsException();
		}
		return user.getId();
	}
}
