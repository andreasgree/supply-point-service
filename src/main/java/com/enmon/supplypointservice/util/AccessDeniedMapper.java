package com.enmon.supplypointservice.util;

import org.springframework.security.access.AccessDeniedException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AccessDeniedMapper implements ExceptionMapper<AccessDeniedException> {
	@Override
	public Response toResponse(AccessDeniedException e) {
		//TODO: logging
		return Response.status(401).build();
	}
}
